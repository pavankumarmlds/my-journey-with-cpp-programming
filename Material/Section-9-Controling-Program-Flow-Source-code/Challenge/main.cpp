// Section 9
// Challenge
/*    
    This challenge is about using a collection (list) of integers and allowing the user
    to select options from a menu to perform operations on the list.
    
    Your program should display a menu options to the user as follows:
    
        P - Print numbers
        A - Add a number
        M - Display mean of the numbers
        S - Display the smallest number
        L - Display the largest number
        Q - Quit

    Enter your choice:
    
    The program should only accept valid choices from the user, both upper and lowercase selections should be allowed.
    If an illegal choice is made, you should display, "Unknown selection, please try again" and the menu options should be
    displayed again.


    If the user enters 'P' or 'p', you should display all of the elements (ints) in the list.
    If the list is empty you should display "[] - the list is empty"
    If the list is not empty then all the list element should be displayed inside square brackets separated by a space. 
    For example, [ 1 2 3 4 5 ]
        
    If the user enters 'A' or 'a' then you should prompt the user for an integer to add to the list 
    which you will add to the list and then display it was added. For example, if the user enters 5
    You should display, "5 added".
    Duplicate list entries are OK

    If the user enters 'M' or 'm'  you should calculate the mean or average of the elements in the list and display it.
    If the list is empty you should display, "Unable to calculate the mean - no data"

    If the user enters 'S' or 's' you should display the smallest element in the list.
    For example, if the list contains [2 4 5 1],  you should display, "The smallest number is 1"
    If the list is empty you should display, "Unable to determine the smallest number - list is empty"

    If the user enters 'L' or 'l' you should display the largest element in the list
    For example, if the list contains [2 4 5 1], you should display, "The largest number is 5"
    If the list is empty you should display, "Unable to determine the largest number - list is empty"

    If the user enters 'Q' or 'q' then you should display 'Goodbye" and the program should terminate.

    Before you begin. Write out the steps you need to take and decide in what order they should be done.
    Think about what loops you should use as well as what you will use for your selection logic.

    This exercise can be challenging! It may likely take a few attempts before you complete it -- that's OK!

    Finally, be sure to test your program as you go and at the end.

    Hint: Use a vector!

    Additional functionality if you wish to extend this program.

    - search for a number in the list and if found display the number of times it occurs in the list
    - clearing out the list (make it empty again) (Hint: the vector class has a .clear() method)
    - don't allow duplicate entries
    - come up with your own ideas!

    Good luck!

*/
#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

int main() {

    vector <int> numbers{};

    char user_choice {};
    do{
        cout << "Available menu options:" << endl;
        cout << "P - Print numbers" << endl;
        cout << "A - Add a number" << endl;
        cout << "M - Display the mean of all numbers" << endl;
        cout << "S - Display the smallest number" << endl;
        cout << "L - Display the largest number" << endl;
        cout << "F - Find a number" << endl;
        cout << "C - Clear the list" << endl;
        cout << "Q - Quit" << endl << endl;
        
        cout << "Enter your choice: ";
        cin >> user_choice;
        cout << endl;

        switch (user_choice){

            case 'P':
            case 'p':
            {
                if (numbers.size() == 0)
                    cout << "[] - The list is empty" << endl << endl;
                else{
                    cout << "[";
                    for (auto number: numbers){
                        cout << number << " ";
                    }
                    cout << "]" << endl;
                    cout << endl;
                }
                continue;
            }
            
            case 'A' : 
            case 'a':
            {
                int num_add {};
                cout << "Enter an integer to add it to the list: ";
                cin >> num_add;

                bool num_identifed {false};

                for(auto num: numbers){
                    if(num_add == num){
                        num_identifed = true;
                        break;
                    }
                }
                if (num_identifed)
                    cout << num_add << " is already present in list and is not added" << endl;
                else {
                    numbers.push_back(num_add);
                    cout << num_add << " is added to the list" << endl;
                }
                cout << endl;
                continue;
            }

            case 'M': 
            case 'm':
            {
                cout << setprecision(2);
                double total {};
                if (numbers.size() != 0){
                    for(auto number: numbers)
                        total += number;
                    cout << "The mean calculated is: " << total/numbers.size() << endl;
                }
                else
                    cout << "Unable to calculate the mean - Data Unavailable" << endl;

                cout << endl;
                continue;
            }

            case 'S':
            case 's':
            {
                int small_num {};
                small_num = numbers.at(0);
                for (auto number: numbers){
                    if (number < small_num)
                        small_num = number;
                }   
                cout << "The smallest number present in the list is: " << small_num << endl;
                cout << endl;
                continue;
            }

            case 'L':
            case 'l':
            {
                int large_num {};
                large_num = numbers.at(0);
                for (auto number: numbers){
                    if (number > large_num)
                        large_num = number;
                }   
                cout << "The smallest number present in the list is: " << large_num << endl;
                cout << endl;
                continue;
            }
            
            case 'F':
            case 'f':
            {
                if(numbers.size() == 0)
                    cout << "List is empty, please add numbers before you perform a search" << endl;
                else{
                    cout << "Enter a number to find in the list: ";
                    int num_to_find {};
                    cin >> num_to_find;

                    int index{};
                    bool num_found {false};
                    for (index = 0; index < numbers.size(); ++index){
                        if (numbers[index] == num_to_find){
                            num_found = true;
                            break;
                        }
                    }

                    if (num_found)
                        cout << num_to_find << " is found at index " << index << endl;
                    else
                        cout << num_to_find << " is not found in the list " << endl;
                }

                cout << endl;
                continue;
            }

            case 'C':
            case 'c':
            {
                numbers.clear();
                cout << "The list is cleared" << endl;
                cout << "Current List: ";
                if (numbers.size() == 0)
                    cout << "[]" << endl;
                cout << endl;
                continue;
            }

            case 'q':
            case 'Q':
                cout << "Quitting..." << endl << endl;
                break;

            default:
                cout << "Invalid Choice" << endl;
        }

    } while (user_choice != 'Q' && user_choice != 'q');
    
    
    return 0;
}